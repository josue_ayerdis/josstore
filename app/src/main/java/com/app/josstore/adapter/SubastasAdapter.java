package com.app.josstore.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.ResourceCursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.app.josstore.R;
import com.app.josstore.activity.ItemActivity;
import com.app.josstore.activity.JosStoreBaseActivity;
import com.app.josstore.data.DataBaseManager;
import com.app.josstore.dialogFragment.PujarDialog;
import com.app.josstore.lib.OnclickCustom;
import com.app.josstore.lib.SessionUsuario;
import com.app.josstore.lib.UtilsJStore;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class SubastasAdapter extends ResourceCursorAdapter {

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    long startTime;
    SessionUsuario sessionUsuario;
    JosStoreBaseActivity mActivity;
    private LayoutInflater inflater;

    public SubastasAdapter(Context context, int layout, Cursor cursor, int flags) {
        super(context, layout, cursor, flags);
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        sessionUsuario = new SessionUsuario(context);
    }

    private static class ViewHolder  {
        TextView tw_name_item;
        TextView tw_description;
        TextView tw_price;
        TextView tw_bids;
        TextView tw_time;
        ImageView iw_item;
        Button btn_pujar;
        CountDownTimer time;
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = inflater.inflate(R.layout.layout_item_home_row, null);
        ViewHolder holder = new ViewHolder();

        holder.tw_name_item = (TextView) view.findViewById(R.id.tw_name_item);
        holder.tw_description = (TextView) view.findViewById(R.id.tw_description);
        holder.tw_price = (TextView) view.findViewById(R.id.tw_price);
        holder.tw_bids = (TextView) view.findViewById(R.id.tw_bids);
        holder.tw_time = (TextView) view.findViewById(R.id.tw_time);
        holder.iw_item = (ImageView) view.findViewById(R.id.iw_item);
        holder.btn_pujar = (Button) view.findViewById(R.id.btn_pujar);

        view.setTag(holder);
        return view;
    }

    public void setmActivity(JosStoreBaseActivity mActivity) {
        this.mActivity = mActivity;
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        final ViewHolder holder = (ViewHolder) view.getTag();

        final int id_item = cursor.getInt(cursor.getColumnIndex(DataBaseManager.TBI_ID));
        int id_user = cursor.getInt(cursor.getColumnIndex(DataBaseManager.TBI_ID_USER));
        final String nombre = cursor.getString(cursor.getColumnIndex(DataBaseManager.TBI_NAME));
        String fecha = cursor.getString(cursor.getColumnIndex(DataBaseManager.TBI_DATE_END));
        final Double precio = cursor.getDouble(cursor.getColumnIndex(DataBaseManager.TBI_PRICE));
        String foto_path = cursor.getString(cursor.getColumnIndex(DataBaseManager.TBI_IMG));
        String desc = cursor.getString(cursor.getColumnIndex(DataBaseManager.TBI_DESC));
        int bids = cursor.getInt(cursor.getColumnIndex("bids"));

        //Log.i("date_end",fecha+"");

        holder.tw_name_item.setText(nombre);
        holder.tw_description.setText(desc);
        holder.tw_price.setText("$"+precio);

        if(bids>0){
            if(bids>1) {
                holder.tw_bids.setText(bids + " postores");
            }else{
                holder.tw_bids.setText(bids + " postor");
            }

            holder.tw_bids.setVisibility(View.VISIBLE);
        }else{
            holder.tw_bids.setVisibility(View.GONE);
        }

        if(foto_path!=null && !foto_path.isEmpty()) {
            //Log.v("file_img", foto_path);
            Bitmap img = UtilsJStore.getImageLocal(foto_path);
            if (img != null) {
                holder.iw_item.setImageBitmap(img);
            }
        }

        if (holder.time != null) {
            holder.time.cancel();
        }

        try {
            Calendar cal = Calendar.getInstance();
            cal.setTime(sdf.parse(fecha));
            long millis = cal.getTimeInMillis();
            startTime = System.currentTimeMillis();

            holder.time = new CountDownTimer(millis, 1000) {

                public void onTick(long millisUntilFinished) {
                    startTime = startTime-1;
                    Long serverUptimeSeconds = (millisUntilFinished - startTime) / 1000;

                    long daysLeft = serverUptimeSeconds / 86400;
                    long hoursLeft = (serverUptimeSeconds % 86400) / 3600;
                    long minutesLeft = ((serverUptimeSeconds % 86400) % 3600) / 60;

                    if(daysLeft>0) {
                        if(daysLeft>1) {
                            holder.tw_time.setText(daysLeft + " dias " + hoursLeft + " hrs");
                        }else{
                            holder.tw_time.setText(daysLeft + " dia " + hoursLeft + " hrs");
                        }
                    }else{
                        if(minutesLeft>0) {
                            holder.tw_time.setText(hoursLeft + " hrs " + minutesLeft + " mins");
                        }else{
                            holder.tw_time.setText("Finalizada");
                            holder.btn_pujar.setVisibility(View.GONE);
                        }
                    }
                }

                public void onFinish() {
                    holder.tw_time.setText("Finalizada");
                    holder.btn_pujar.setVisibility(View.GONE);
                }

            }.start();

            holder.tw_time.setVisibility(View.VISIBLE);
        }catch (ParseException e){
            holder.tw_time.setVisibility(View.GONE);
            e.printStackTrace();
        }


        if(sessionUsuario.isLoggedIn() && sessionUsuario.getUserInstance().getId() == id_user ){
            holder.btn_pujar.setOnClickListener(null);
            holder.btn_pujar.setVisibility(View.GONE);
        }else{
            holder.btn_pujar.setVisibility(View.VISIBLE);

            holder.btn_pujar.setOnClickListener(new OnclickCustom() {
                @Override
                public void onClickC(View view) {
                    if(mActivity!=null) {
                        DialogFragment pujaDialog = new PujarDialog();

                        Bundle args = new Bundle();
                        args.putInt("id_item", id_item);
                        args.putDouble("precio_item", precio);
                        pujaDialog.setArguments(args);

                        /** Open dialog **/
                        mActivity.getSupportFragmentManager().beginTransaction().add(pujaDialog, "dialog_puja").commitAllowingStateLoss();
                    }
                }
            });
        }



        // Evento item
        view.setOnClickListener(new OnclickCustom() {
            @Override
            public void onClickC(View view) {
                Intent i_item = new Intent(mActivity,ItemActivity.class);
                i_item.putExtra("id_item",id_item);
                i_item.putExtra("name_item",nombre);

                mActivity.startActivity(i_item);
            }
        });

    }

}
