package com.app.josstore.adapter;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.ResourceCursorAdapter;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.app.josstore.R;
import com.app.josstore.activity.ItemActivity;
import com.app.josstore.activity.JosStoreBaseActivity;
import com.app.josstore.activity.MisPujasActivity;
import com.app.josstore.data.DataBaseManager;
import com.app.josstore.dialogFragment.PujarDialog;
import com.app.josstore.lib.OnclickCustom;
import com.app.josstore.lib.UtilsJStore;

public class MisPujasAdapter extends ResourceCursorAdapter {

    JosStoreBaseActivity mActivity;
    DataBaseManager db;

    public MisPujasAdapter(Context context, int layout, Cursor cursor, int flags) {
        super(context, layout, cursor, flags);
        //sessionUsuario = new SessionUsuario(context);
        mActivity = (JosStoreBaseActivity) context;
        db = new DataBaseManager(mActivity);
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        TextView tw_name_item = (TextView) view.findViewById(R.id.tw_name_item);
        TextView tw_puja = (TextView) view.findViewById(R.id.tw_puja);
        TextView tw_ganastes = (TextView) view.findViewById(R.id.tw_ganastes);
        ImageView iw_item = (ImageView) view.findViewById(R.id.iw_item);
        Button btn_modifcar = (Button) view.findViewById(R.id.btn_modifcar);
        Button btn_eliminar = (Button) view.findViewById(R.id.btn_eliminar);

        final int id_bid = cursor.getInt(cursor.getColumnIndex(DataBaseManager.TBP_ID));
        final int id_item = cursor.getInt(cursor.getColumnIndex(DataBaseManager.TBP_ID_ITEM));
        int id_user = cursor.getInt(cursor.getColumnIndex(DataBaseManager.TBP_ID_USER));
        final String nombre = cursor.getString(cursor.getColumnIndex(DataBaseManager.TBI_NAME));
        Double puja = cursor.getDouble(cursor.getColumnIndex(DataBaseManager.TBP_PRICE));
        final Double price_item = cursor.getDouble(cursor.getColumnIndex("price_item"));
        String foto_path = cursor.getString(cursor.getColumnIndex(DataBaseManager.TBI_IMG));
        String status = cursor.getString(cursor.getColumnIndex(DataBaseManager.TBP_STATUS));

        tw_name_item.setText(nombre);
        tw_puja.setText("Puja: $"+puja);

        if(foto_path!=null && !foto_path.isEmpty()) {
            Bitmap img = UtilsJStore.getImageLocal(foto_path);
            if (img != null) {
                iw_item.setImageBitmap(img);
            }
        }

        if( status.equalsIgnoreCase("alta") ) {
            btn_modifcar.setOnClickListener(new OnclickCustom() {
                @Override
                public void onClickC(View view) {
                    DialogFragment pujaDialog = new PujarDialog();

                    Bundle args = new Bundle();
                    args.putInt("id_item", id_item);
                    args.putDouble("precio_item", price_item);
                    args.putBoolean("is_update", true);
                    pujaDialog.setArguments(args);

                    /** Open dialog **/
                    mActivity.getSupportFragmentManager().beginTransaction().add(pujaDialog, "dialog_puja").commitAllowingStateLoss();
                }
            });

            btn_eliminar.setOnClickListener(new OnclickCustom() {
                @Override
                public void onClickC(View view) {

                    new AlertDialog.Builder(mActivity)
                            .setTitle("Eliminar puja")
                            .setMessage("¿Desea eliminar esta puja?")
                            .setPositiveButton("SI", new DatePickerDialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (UtilsJStore.isFirstClick()) {
                                        if (db.delete_puja(id_bid)) {
                                            //Update mis pujas
                                            Intent i_update_mp = new Intent(MisPujasActivity.ACTION_UPDATE_PUJAS);
                                            mActivity.sendBroadcast(i_update_mp);

                                            dialog.dismiss();
                                            UtilsJStore.show_accept_msg(mActivity, "Puja Eliminada");
                                        } else {
                                            UtilsJStore.show_accept_msg(mActivity, "Hubo un problema al eliminar la puja");
                                        }
                                    }
                                }
                            })
                            .setNegativeButton("NO", null)
                            .show();

                }
            });

            btn_eliminar.setVisibility(View.VISIBLE);
            btn_modifcar.setVisibility(View.VISIBLE);
            tw_ganastes.setVisibility(View.GONE);
        }else{
            btn_eliminar.setVisibility(View.GONE);
            btn_modifcar.setVisibility(View.GONE);
            tw_ganastes.setVisibility(View.VISIBLE);
        }

        // Evento item
        view.setOnClickListener(new OnclickCustom() {
            @Override
            public void onClickC(View view) {
                Intent i_item = new Intent(mActivity,ItemActivity.class);
                i_item.putExtra("id_item",id_item);
                i_item.putExtra("name_item",nombre);

                mActivity.startActivity(i_item);
            }
        });

    }

}
