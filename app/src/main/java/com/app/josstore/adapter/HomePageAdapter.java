package com.app.josstore.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.app.josstore.fragment.MisSubastasFragment;
import com.app.josstore.fragment.SubastasFragment;

public class HomePageAdapter extends FragmentPagerAdapter {

    String tabs[] = {"Subastas","Mis subastas"};

    public HomePageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabs[position];
    }

    @Override
    public Fragment getItem(int pos) {
        switch(pos) {

            case 0: return SubastasFragment.newInstance();
            case 1: return MisSubastasFragment.newInstance();

            default: return SubastasFragment.newInstance();
        }
    }

    @Override
    public int getCount() {
        return tabs.length;
    }

}
