package com.app.josstore.broadcast;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.app.josstore.activity.HomeActivity;
import com.app.josstore.activity.MisPujasActivity;

/**
 * Created by josue on 2/6/2018.
 */

public class NotificationOpenReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if(intent.getExtras()!=null) {
            int notif_id = intent.getExtras().getInt("notif_id", 0);

            switch (notif_id) {

                case 2:
                    Log.i("notif","entro");

                    Intent intentHome = new Intent(context, HomeActivity.class);
                    intentHome.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intentHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(intentHome);

                    Intent intentPujas = new Intent(context, MisPujasActivity.class);
                    context.startActivity(intentPujas);
                break;

            }


            // Quito la notificación del status bar
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(notif_id);
        }
    }

    public static PendingIntent createOnOpenIntent(Context context, int notificationId) {
        Intent intent = new Intent(context, NotificationOpenReceiver.class);
        intent.setAction(notificationId+"");
        intent.putExtra("notif_id",notificationId);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        return PendingIntent.getBroadcast(context.getApplicationContext(), notificationId, intent, 0);
    }

}