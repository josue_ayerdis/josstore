package com.app.josstore.services;


import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.app.josstore.data.DataBaseManager;
import com.app.josstore.lib.SessionUsuario;
import java.util.Timer;
import java.util.TimerTask;

public class CheckBids extends Service {

    private Timer timer;
    private TimerTask timerTask;
    private int total_sec = 60;


    public CheckBids(Context applicationContext) {
        super();
    }

    public CheckBids() {}

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        startTimer();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Intent broadcastIntent = new Intent("com.app.josstore.ActivityRecognition.RestartSensor");
        sendBroadcast(broadcastIntent);
        stoptimertask();
    }


    public void startTimer() {
        SessionUsuario sessionUsuario = new SessionUsuario(CheckBids.this);

        if(sessionUsuario.isLoggedIn()){
            //set a new Timer
            timer = new Timer();

            //initialize the TimerTask's job
            initializeTimerTask();

            //schedule the timer, to wake up every 1 second
            timer.schedule(timerTask, 1000, total_sec * 1000); //
        }
    }

    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                Log.i("notif","servicio check");

                DataBaseManager db = new DataBaseManager(CheckBids.this);

                db.verificar_pujas();
            }
        };
    }

    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
