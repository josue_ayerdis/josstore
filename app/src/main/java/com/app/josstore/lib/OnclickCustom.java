package com.app.josstore.lib;

import android.view.View;

/**
 * Created by Josue on 27/1/17.
 */

public abstract class OnclickCustom implements View.OnClickListener {

    @Override
    public void onClick(View view) {
        if(UtilsJStore.isFirstClick()){
            onClickC(view);
        }
    }

    public abstract void onClickC(View view);

}
