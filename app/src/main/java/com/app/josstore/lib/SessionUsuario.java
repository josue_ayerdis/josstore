package com.app.josstore.lib;

import android.content.Context;
import android.content.SharedPreferences;
import com.app.josstore.model.Usuario;
import com.google.gson.Gson;

/**
 * Created by Degusta-JA on 02/17/2017.
 */

public class SessionUsuario {

    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "JossStoreSessionPref";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    // Json String with the user data
    public static final String KEY_USER_DATA = "userData";

    // Constructor
    public SessionUsuario(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     * */
    public void createLoginSession(Usuario user_valid){

        Gson gson = new Gson();
        String jsonUserData = gson.toJson(user_valid);

        // Solo lo logeo si llega la sesion del usuario
        if( user_valid!=null && user_valid.getId()>0 ) {

            // Storing email in pref
            editor.putString(KEY_USER_DATA, jsonUserData);

            // Storing login value as TRUE
            editor.putBoolean(IS_LOGIN, true);

            // commit changes
            editor.commit();
        }

    }

    public void logoutUser(){
        logoutInApp();
    }

    public void logoutInApp(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
    }

    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }

    /**
     *
     * Get User Object data
     *
     */
    public Usuario getUserInstance(){
        Gson gson = new Gson();
        String json_user = pref.getString(KEY_USER_DATA,null);
        return gson.fromJson(json_user,Usuario.class);
    }

    public void updateUserData(Usuario user){
        if(isLoggedIn()) {

            Gson gson = new Gson();
            String json_user_data = gson.toJson(user);

            if (user != null && user.getId() > 0) {
                // Storing email in pref
                editor.putString(KEY_USER_DATA, json_user_data);
                editor.commit();
            }

        }
    }



}
