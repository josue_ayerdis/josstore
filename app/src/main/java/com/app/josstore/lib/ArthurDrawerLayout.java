package com.app.josstore.lib;

import android.content.Context;
import android.support.v4.widget.DrawerLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by Josue on 7/2/16.
 */
public class ArthurDrawerLayout extends DrawerLayout {

    private boolean IntercepterDisallowed = false;

    //Required default constructors
    public ArthurDrawerLayout(Context context){super(context);}
    public ArthurDrawerLayout(Context context, AttributeSet attrs) {super(context, attrs);}

    public ArthurDrawerLayout(Context context, AttributeSet attrs, int defStyle) {super(context, attrs, defStyle);}


    @Override
    public void requestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        /*
         * We will store the decision of the inner view, whether the intercepter is disallowed or not.
         */
        IntercepterDisallowed = disallowIntercept;
        super.requestDisallowInterceptTouchEvent(disallowIntercept);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        /*
         * When multi touch happens, handle it.
         */
        if (ev.getPointerCount() >1 && IntercepterDisallowed) {
            requestDisallowInterceptTouchEvent(false);
            boolean handled =super.dispatchTouchEvent(ev);
            requestDisallowInterceptTouchEvent(true);
            return handled;
        } else {
            return super.dispatchTouchEvent(ev);
        }
    }
}
