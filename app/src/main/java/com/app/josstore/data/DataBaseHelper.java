package com.app.josstore.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "j_store.sqlite"; // nombre del archivo de la base de datos.
    private static final int DB_SCHEMA_VERSION = 1;

    public DataBaseHelper(Context context) {
        super(context, DB_NAME, null, DB_SCHEMA_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        /**
         * Creo las tablas
         */
        sqLiteDatabase.execSQL(DataBaseManager.CREATE_TABLE_U);
        sqLiteDatabase.execSQL(DataBaseManager.CREATE_TABLE_I);
        sqLiteDatabase.execSQL(DataBaseManager.CREATE_TABLE_P);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

    }
}
