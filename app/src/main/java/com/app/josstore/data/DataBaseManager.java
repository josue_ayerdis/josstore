package com.app.josstore.data;

import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.app.josstore.R;
import com.app.josstore.broadcast.NotificationOpenReceiver;
import com.app.josstore.lib.SessionUsuario;
import com.app.josstore.model.Usuario;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DataBaseManager {

    private DataBaseHelper helper;
    private SQLiteDatabase db;
    private Cursor cursor;
    private Context context;

    // Nombre de la tabla de la base de datos.
    public static final String TABLE_USER = "Users";
    public static final String TABLE_ITEMS = "Items";
    public static final String TABLE_PUJAS = "Pujas";

    // Se definen los nombres de las columnas de la tabla Usuario.
    public static final String TBU_ID       = "_id";
    public static final String TBU_EMAIL    = "email";
    public static final String TBU_USERNAME = "nombre_usuario";
    public static final String TBU_PASS     = "password";

    // Se definen los nombres de las columnas de la tabla Articulos.
    public static final String TBI_ID       = "_id";
    public static final String TBI_IMG      = "img";
    public static final String TBI_NAME     = "nombre";
    public static final String TBI_DATE_REG = "fecha_reg";
    public static final String TBI_DATE_END = "fecha_fin";
    public static final String TBI_PRICE    = "precio";
    public static final String TBI_DESC     = "descripcion";
    public static final String TBI_ID_USER  = "id_user";

    // Se definen los nombres de las columnas de la tabla Pujas para Articulos.
    public static final String TBP_ID = "_id";
    public static final String TBP_ID_ITEM = "id_item";
    public static final String TBP_ID_USER = "id_user";
    public static final String TBP_PRICE = "precio";
    public static final String TBP_DATE = "fecha";
    public static final String TBP_STATUS = "status";

    public static final String CREATE_TABLE_U = "CREATE TABLE " +TABLE_USER+ " ("
            +TBU_ID    + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            +TBU_EMAIL + " VARCHAR(255) NOT NULL UNIQUE, "
            +TBU_USERNAME + " VARCHAR(255) NOT NULL, "
            +TBU_PASS   + " VARCHAR(255) NOT NULL); ";

    public static final String CREATE_TABLE_I = "CREATE TABLE " +TABLE_ITEMS+ " ("
            +TBI_ID    + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            +TBI_ID_USER + " INTEGER, "
            +TBI_IMG + " VARCHAR(255) NOT NULL, "
            +TBI_NAME + " VARCHAR(255) NOT NULL, "
            +TBI_DATE_REG + " TEXT DEFAULT (DATETIME(CURRENT_TIMESTAMP, 'LOCALTIME')), "
            +TBI_DATE_END + " TEXT, "
            +TBI_PRICE + " DOUBLE, "
            +TBI_DESC + " VARCHAR(255) ); ";

    public static final String CREATE_TABLE_P = "CREATE TABLE " +TABLE_PUJAS+ " ("
            +TBP_ID    + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            +TBP_ID_ITEM + " INTEGER, "
            +TBP_ID_USER + " INTEGER, "
            +TBP_DATE + " TEXT DEFAULT (DATETIME(CURRENT_TIMESTAMP, 'LOCALTIME')), "
            +TBP_PRICE + " DOUBLE, "
            +TBP_STATUS + " VARCHAR(255) ); ";


    public DataBaseManager(Context context) {
        this.context = context;
        helper = new DataBaseHelper(context);
        //si la base de datos no existe getwritabledatabse la crea y la devuelve en escritura
        // si existe solo la devuelve.
        db = helper.getWritableDatabase();
    }



    public boolean login_usuario(String nombre_usuario,String password){
        String sql_login = "SELECT * FROM "+TABLE_USER+" WHERE "+TBU_USERNAME+" = '"+nombre_usuario+"' AND "+TBU_PASS+" = '"+password+"'";
        cursor = db.rawQuery(sql_login,null);
        boolean isEmpty = cursor.getCount() < 1;

        if(!isEmpty){
            cursor.moveToFirst();

            Usuario usuario = new Usuario();
            usuario.setId(cursor.getInt(cursor.getColumnIndex(TBU_ID)));
            usuario.setNombre_usuario(cursor.getString(cursor.getColumnIndex(TBU_USERNAME)));
            usuario.setEmail(cursor.getString(cursor.getColumnIndex(TBU_EMAIL)));

            // Crear la sesion del usuario
            SessionUsuario sessionUsuario = new SessionUsuario(context);
            sessionUsuario.createLoginSession(usuario);

            return true;
        }else{
            return false;
        }

    }

    public boolean existe_usuario(String email,String nombre_usuario){
        String sql_user = "SELECT * FROM "+TABLE_USER+" WHERE "+TBU_USERNAME+" = '"+nombre_usuario+"' AND "+TBU_EMAIL+" = '"+email+"'";

        cursor = db.rawQuery(sql_user,null);
        boolean isEmpty = cursor.getCount() < 1;

        if(!isEmpty){
            return true;
        }else{
            return false;
        }
    }

    public boolean registrar_usuario(String nombre_usuario, String email, String password){
        ContentValues values = new ContentValues();
        values.put(TBU_EMAIL,email);
        values.put(TBU_USERNAME,nombre_usuario);
        values.put(TBU_PASS,password);

        if( db.insert(TABLE_USER,null,values) == -1){
            return false;
        }else{
            login_usuario(nombre_usuario,password); // lo logeo
            return true;
        }
    }

    public boolean insert_item(int id_user,String nombre,String precio,String descripcion,String foto,String fecha){
        ContentValues values = new ContentValues();
        values.put(TBI_ID_USER,id_user);
        values.put(TBI_NAME,nombre);
        values.put(TBI_PRICE,precio);
        values.put(TBI_DESC,descripcion);
        values.put(TBI_IMG,foto);

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Calendar cal = Calendar.getInstance();
            cal.setTime(sdf.parse(fecha));
            Date date = cal.getTime();
            String fecha_local = sdf.format(date);

            values.put(TBI_DATE_END, fecha_local);
        }catch (ParseException e){
            e.printStackTrace();
        }

        if( db.insert(TABLE_ITEMS,null,values) == -1){
            return false;
        }else{
            return true;
        }
    }

    public Cursor get_subastas_activas(){
        String sql_subastas = "SELECT "+TABLE_ITEMS+".*,p.bids FROM "+TABLE_ITEMS+
                " LEFT JOIN (SELECT *,COUNT(*) as bids FROM "+TABLE_PUJAS+" WHERE "+TBP_STATUS+"='alta' GROUP BY "+TBP_ID_ITEM+" ) p ON "+TABLE_ITEMS+"."+TBI_ID+" = p."+TBP_ID_ITEM+
                " WHERE strftime('%s',"+TABLE_ITEMS+"."+TBI_DATE_END+") > strftime('%s','now','localtime') "+
                " ORDER BY strftime('%s',"+TBI_DATE_REG+") DESC";

        Log.i("sql",sql_subastas);

        return db.rawQuery(sql_subastas,null);
    }

    public Cursor get_subastas_user(int id_user){
        String sql_subastas = "SELECT "+TABLE_ITEMS+".*,p.bids FROM "+TABLE_ITEMS+
                " LEFT JOIN (SELECT *,COUNT(*) as bids FROM "+TABLE_PUJAS+" WHERE "+TBP_STATUS+"='alta' GROUP BY "+TBP_ID_ITEM+" ) p ON "+TABLE_ITEMS+"."+TBI_ID+" = p."+TBP_ID_ITEM+
                " WHERE strftime('%s',"+TABLE_ITEMS+"."+TBI_DATE_END+") > strftime('%s','now','localtime') AND "+TABLE_ITEMS+"."+TBI_ID_USER+"="+id_user+
                " ORDER BY strftime('%s',"+TBI_DATE_REG+") DESC";

        return db.rawQuery(sql_subastas,null);
    }


    public boolean crear_puja(int id_user,int id_item,double precio){
        ContentValues values = new ContentValues();
        values.put(TBP_ID_USER,id_user);
        values.put(TBP_ID_ITEM,id_item);
        values.put(TBP_PRICE,precio);
        values.put(TBP_STATUS,"alta");

        if( db.insert(TABLE_PUJAS,null,values) == -1){
            return false;
        }else{
            return true;
        }
    }

    public boolean existe_puja(int id_user,int id_item){
        String sql_puja = "SELECT * FROM "+TABLE_PUJAS+" WHERE "+TBP_ID_ITEM+" = "+id_item+" AND "+TBP_ID_USER+" = "+id_user+" AND "+TBP_STATUS+"='alta'";

        cursor = db.rawQuery(sql_puja,null);
        boolean isEmpty = cursor.getCount() < 1;

        if(!isEmpty){
            return true;
        }else{
            return false;
        }
    }

    public boolean update_puja(int id_user,int id_item,double precio){
        ContentValues values = new ContentValues();
        values.put(TBP_PRICE,precio);

        if( db.update(TABLE_PUJAS,values,TBP_ID_ITEM+"="+id_item+" AND "+TBP_ID_USER+"="+id_user+" AND "+TBP_STATUS+"='alta'",null) == -1){
            return false;
        }else{
            return true;
        }
    }

    public Cursor get_item(int id_item){

        String sql_item = "SELECT "+TABLE_ITEMS+".*,p.bids FROM "+TABLE_ITEMS+
                " LEFT JOIN (SELECT *,COUNT(*) as bids FROM "+TABLE_PUJAS+" ) p ON "+TABLE_ITEMS+"."+TBI_ID+" = p."+TBP_ID_ITEM+
                " WHERE  "+TABLE_ITEMS+"."+TBI_ID+" = "+id_item;

        return db.rawQuery(sql_item,null);
    }


    public Cursor get_user_pujas(int id_user){
        String sql_pujas = "SELECT "+TABLE_PUJAS+".*,"+TABLE_ITEMS+"."+TBI_NAME+","+TABLE_ITEMS+"."+TBI_IMG+","+TABLE_ITEMS+"."+TBI_PRICE+" AS price_item FROM "+TABLE_PUJAS+
                " INNER JOIN "+TABLE_ITEMS+" ON "+TABLE_PUJAS+"."+TBP_ID_ITEM+"="+TABLE_ITEMS+"."+TBI_ID+
                " WHERE "+TABLE_PUJAS+"."+TBP_ID_USER+"="+id_user+" AND "+TABLE_PUJAS+"."+TBP_STATUS+"<>'baja'";

        return db.rawQuery(sql_pujas,null);
    }

    public boolean delete_puja(int id_bid){
        ContentValues values = new ContentValues();
        values.put(TBP_STATUS,"baja");

        if( db.update(TABLE_PUJAS,values,TBP_ID+"="+id_bid,null) == -1){
            return false;
        }else{
            return true;
        }
    }

    public boolean ganar_puja(int id_bid){
        ContentValues values = new ContentValues();
        values.put(TBP_STATUS,"win");

        if( db.update(TABLE_PUJAS,values,TBP_ID+"="+id_bid,null) == -1){
            return false;
        }else{
            return true;
        }
    }

    public void verificar_pujas(){
        String sql_subastas_pasadas = "SELECT * FROM "+TABLE_ITEMS+
                " WHERE strftime('%s',"+TABLE_ITEMS+"."+TBI_DATE_END+") <= strftime('%s','now','localtime')";

        Cursor c_subastas_terminadas = db.rawQuery(sql_subastas_pasadas,null);

        try {
            while (c_subastas_terminadas.moveToNext()) {

                int id_item = c_subastas_terminadas.getInt(c_subastas_terminadas.getColumnIndex(DataBaseManager.TBI_ID));

                if(id_item>0){

                    Cursor winner = check_winner_item(id_item);

                    if(winner!=null && winner.getCount()>0){
                        /** NO hacer nada **/
                    }else{
                        /** Marcar ganador de subasta **/
                        mark_winner_item(id_item);
                    }

                }

            }
        } finally {
            c_subastas_terminadas.close();
        }

    }


    public Cursor check_winner_item(int id_item){
        String sql_winner = "SELECT * FROM "+TABLE_PUJAS+
                " WHERE "+TBP_ID_ITEM+" = "+id_item+" AND "+TBP_STATUS+" = 'win' ";

        return db.rawQuery(sql_winner,null);
    }

    public void mark_winner_item(int id_item){
        String sql_pujas_item = "SELECT * FROM "+TABLE_PUJAS+
                " WHERE "+TBP_ID_ITEM+" = "+id_item+" AND "+TBP_STATUS+" = 'alta' "+
                " ORDER BY "+TBP_PRICE+" DESC LIMIT 1";

        Cursor cursor = db.rawQuery(sql_pujas_item,null);

        try {
            if (cursor.moveToFirst()){
                int id_bid = cursor.getInt(cursor.getColumnIndex(DataBaseManager.TBP_ID));
                int id_user = cursor.getInt(cursor.getColumnIndex(DataBaseManager.TBP_ID_USER));

                if( ganar_puja(id_bid) ){

                    /** Enviar notificacion al usuario ganador **/
                    SessionUsuario sessionUsuario = new SessionUsuario(context);

                    if(sessionUsuario.isLoggedIn() && sessionUsuario.getUserInstance().getId() == id_user){
                        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, "JOSS_STORE_CHANNEL_1");

                        int notif_id = 2;

                        notificationBuilder.setSmallIcon(R.mipmap.ic_launcher)
                                .setColor(ContextCompat.getColor(context,R.color.black))
                                .setContentTitle("GANASTES")
                                .setStyle(new NotificationCompat.BigTextStyle().bigText("Ganastes una de tus pujas realizada"))
                                .setContentText("Ganastes una de tus pujas realizada")
                                .setSound(defaultSoundUri);

                        /** Default intent **/
                        notificationBuilder.setContentIntent(NotificationOpenReceiver.createOnOpenIntent(context, notif_id));


                        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.notify(notif_id, notificationBuilder.build());

                    }

                }
            }
        }finally {
            cursor.close();
        }

    }




}
