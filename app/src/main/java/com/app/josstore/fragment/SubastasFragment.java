package com.app.josstore.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.app.josstore.R;
import com.app.josstore.adapter.SubastasAdapter;
import com.app.josstore.data.DataBaseManager;

public class SubastasFragment extends FragmentJosStore {

    public static final String ACTION_UPDATE_SUBASTAS = "SubastasFragment.ACTION_UPDATE_SUBASTAS";
    private IntentFilter filter;
    private UpdateReceiver updateReceiver;

    ListView lw_subastas;
    SubastasAdapter adapter;
    DataBaseManager db;
    Cursor cursor;

    public static SubastasFragment newInstance(){
        return new SubastasFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        filter = new IntentFilter();
        filter.addAction(ACTION_UPDATE_SUBASTAS);
        updateReceiver = new UpdateReceiver();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.fragment_subastas, parent, false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {

        db = new DataBaseManager(mActivity);
        cursor = db.get_subastas_activas();

        lw_subastas = (ListView) view.findViewById(R.id.lw_subastas);

        adapter = new SubastasAdapter(mActivity,R.layout.layout_item_home_row,cursor,0);
        adapter.setmActivity(mActivity);
        lw_subastas.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("frag","resume");
        mActivity.registerReceiver(updateReceiver, filter);
    }

    @Override
    public void onDestroy() {
        try{
            mActivity.unregisterReceiver(updateReceiver);
        }catch (IllegalArgumentException e){
            e.printStackTrace();
        }
        super.onDestroy();
    }

    class UpdateReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if( intent.getAction()!=null && intent.getAction().equals(ACTION_UPDATE_SUBASTAS)){
                cursor = db.get_subastas_activas();
                adapter.changeCursor(cursor);
                adapter.notifyDataSetChanged();
            }

        }

    }

}
