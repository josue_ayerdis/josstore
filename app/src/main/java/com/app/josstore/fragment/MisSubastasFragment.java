package com.app.josstore.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.app.josstore.R;
import com.app.josstore.activity.AddItemActivity;
import com.app.josstore.adapter.SubastasAdapter;
import com.app.josstore.data.DataBaseManager;
import com.app.josstore.lib.OnclickCustom;
import com.app.josstore.lib.SessionUsuario;

public class MisSubastasFragment extends FragmentJosStore {

    public static final String ACTION_UPDATE_MIS_SUBASTAS = "MisSubastasFragment.ACTION_UPDATE_MIS_SUBASTAS";
    private IntentFilter filter;
    private UpdateReceiver updateReceiver;

    ListView lw_mis_subastas;
    LinearLayout ll_empty;
    Button btn_crear;
    SubastasAdapter adapter;
    DataBaseManager db;
    Cursor cursor;
    SessionUsuario sessionUsuario;

    public static MisSubastasFragment newInstance(){
        return new MisSubastasFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        filter = new IntentFilter();
        filter.addAction(ACTION_UPDATE_MIS_SUBASTAS);
        updateReceiver = new UpdateReceiver();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.fragment_mis_subastas, parent, false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {

        sessionUsuario = new SessionUsuario(mActivity);

        db = new DataBaseManager(mActivity);
        lw_mis_subastas = (ListView) view.findViewById(R.id.lw_mis_subastas);
        ll_empty = (LinearLayout) view.findViewById(R.id.ll_empty);
        btn_crear = (Button) view.findViewById(R.id.btn_crear);

        if(sessionUsuario.isLoggedIn()) {
            cursor = db.get_subastas_user(sessionUsuario.getUserInstance().getId());

            if(cursor.getCount()>0) {
                adapter = new SubastasAdapter(mActivity, R.layout.layout_item_home_row, cursor, 0);
                adapter.setmActivity(mActivity);
                lw_mis_subastas.setAdapter(adapter);
            }else{
                lw_mis_subastas.setVisibility(View.GONE);
                ll_empty.setVisibility(View.VISIBLE);

                btn_crear.setOnClickListener(new OnclickCustom() {
                    @Override
                    public void onClickC(View view) {
                        Intent iAddItem = new Intent(mActivity,AddItemActivity.class);
                        startActivity(iAddItem);
                    }
                });
            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.registerReceiver(updateReceiver, filter);
    }

    @Override
    public void onDestroy() {
        try{
            mActivity.unregisterReceiver(updateReceiver);
        }catch (IllegalArgumentException e){
            e.printStackTrace();
        }
        super.onDestroy();
    }

    class UpdateReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if( intent.getAction()!=null && intent.getAction().equals(ACTION_UPDATE_MIS_SUBASTAS)){
                if( sessionUsuario!=null && sessionUsuario.isLoggedIn()) {
                    cursor = db.get_subastas_user(sessionUsuario.getUserInstance().getId());
                    if(adapter!=null) {
                        adapter.changeCursor(cursor);
                        adapter.notifyDataSetChanged();
                    }else{
                        adapter = new SubastasAdapter(mActivity, R.layout.layout_item_home_row, cursor, 0);
                        adapter.setmActivity(mActivity);
                        lw_mis_subastas.setAdapter(adapter);

                        lw_mis_subastas.setVisibility(View.VISIBLE);
                        ll_empty.setVisibility(View.GONE);
                    }
                }
            }

        }

    }

}
