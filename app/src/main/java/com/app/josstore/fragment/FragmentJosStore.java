package com.app.josstore.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.app.josstore.activity.JosStoreBaseActivity;
import com.app.josstore.lib.UtilsJStore;

public class FragmentJosStore extends Fragment {

    JosStoreBaseActivity mActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (JosStoreBaseActivity) getActivity();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UtilsJStore.reset_click_time();
    }

}
