package com.app.josstore.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.ListView;
import com.app.josstore.R;
import com.app.josstore.adapter.MisPujasAdapter;
import com.app.josstore.data.DataBaseManager;

public class MisPujasActivity extends JosStoreBaseActivity {

    ListView lw_pujas;
    MisPujasAdapter adapter;
    DataBaseManager db;
    Cursor cursor;

    public static final String ACTION_UPDATE_PUJAS = "MisPujasActivity.ACTION_UPDATE_PUJAS";
    UpdateReceiver updateReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pujas);

        baseCreate(savedInstanceState);

        if (getSupportActionBar() != null) getSupportActionBar().setTitle("");
        setActionbarTitle("Mis pujas");
        alignTitleStart();
        activeDisplayBack();

        /** Update intent **/
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_UPDATE_PUJAS);
        updateReceiver = new UpdateReceiver();
        registerReceiver(updateReceiver,filter);

        lw_pujas = (ListView) findViewById(R.id.lw_pujas);
        db = new DataBaseManager(this);

        if(sessionUsuario.isLoggedIn()) {
            cursor = db.get_user_pujas(sessionUsuario.getUserInstance().id);

            adapter = new MisPujasAdapter(this, R.layout.layout_puja_row, cursor, 0);
            lw_pujas.setAdapter(adapter);
        }

    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(updateReceiver);
        super.onDestroy();
    }

    class UpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction()!=null && intent.getAction().equals(ACTION_UPDATE_PUJAS) && db!=null) {
                if(sessionUsuario.isLoggedIn()) {
                    cursor = db.get_user_pujas(sessionUsuario.getUserInstance().id);
                    adapter.changeCursor(cursor);
                    adapter.notifyDataSetChanged();
                }
            }
        }
    }
}
