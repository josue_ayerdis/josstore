package com.app.josstore.activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.app.josstore.R;
import com.app.josstore.lib.OnclickCustom;
import com.app.josstore.lib.SessionUsuario;
import com.app.josstore.lib.UtilsJStore;

public class JosStoreBaseActivity extends AppCompatActivity {

    public ActionBar mActionBar = null;
    public Toolbar mToolbar = null;
    public boolean backButtonActive;

    /** Panel objects **/
    public String[] mConfigList            = null;
    public LinearLayout mDrawerList        = null;
    public boolean flagPanelOption         = false;
    public DrawerLayout mDrawerLayout      = null;
    public ActionBarDrawerToggle mToggle   = null;
    boolean flag_trigger_logout;

    SessionUsuario sessionUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionUsuario = new SessionUsuario(this);
    }


    public void baseCreate(Bundle savedInstanceState){
        UtilsJStore.reset_click_time();

        init_views();
    }

    public void init_views(){
        /** Toolbar acting like actionbar **/
        mToolbar = (Toolbar) findViewById(R.id.toolbar_jstore);

        if(mToolbar!=null) {
            try {
                setSupportActionBar(mToolbar);
            } catch (Throwable t) {
                /** WTF Samsung 4.2.2 **/
            }

        }
    }

    public void populate_panel(){

        if(mActionBar==null)
            mActionBar = getSupportActionBar();

        if(mConfigList==null)
            mConfigList = getResources().getStringArray(R.array.lista_opts_panel);

        if(mDrawerList == null)
            mDrawerList = (LinearLayout) findViewById(R.id.left_drawer_lista);

        if(sessionUsuario.isLoggedIn()) {
            TextView tw_display_name_panel = (TextView) findViewById(R.id.tw_display_name_panel);

            tw_display_name_panel.setText(sessionUsuario.getUserInstance().getNombre_usuario());
        }

        LayoutInflater inflater = LayoutInflater.from(this);
        Boolean add_option = false;

        for (String opcion : mConfigList) {

            View opcion_panel;
            opcion_panel = inflater.inflate(R.layout.layout_list_item_panel, mDrawerList, false);

            TextView btn_opcion = (TextView) opcion_panel.findViewById(R.id.buttonlista);
            final RelativeLayout button_lista_holder = (RelativeLayout) opcion_panel.findViewById(R.id.button_lista_holder);
            Drawable img_icon = null;

            switch (opcion){

                case "add_item":
                    btn_opcion.setText("Subastar Artículo");
                    img_icon = ContextCompat.getDrawable(this, R.mipmap.ic_plus);
                    add_option = true;

                    button_lista_holder.setOnClickListener(new OnclickCustom() {
                        @Override
                        public void onClickC(View view) {
                            if(mDrawerLayout!=null)mDrawerLayout.closeDrawers();

                            Intent iAddItem = new Intent(JosStoreBaseActivity.this,AddItemActivity.class);
                            startActivity(iAddItem);
                        }
                    });
                    break;

                case "my_bids":
                    btn_opcion.setText("Mis pujas");
                    img_icon = ContextCompat.getDrawable(this, R.mipmap.ic_bids);
                    add_option = true;

                    button_lista_holder.setOnClickListener(new OnclickCustom() {
                        @Override
                        public void onClickC(View view) {
                            if(mDrawerLayout!=null)mDrawerLayout.closeDrawers();

                            Intent iMybids = new Intent(JosStoreBaseActivity.this,MisPujasActivity.class);
                            startActivity(iMybids);
                        }
                    });
                    break;

                case "cerrar_sesion":
                    btn_opcion.setText("Cerrar sesíon");
                    img_icon = ContextCompat.getDrawable(this, R.mipmap.ic_panel_izq_logout);
                    add_option = true;

                    button_lista_holder.setOnClickListener(new OnclickCustom() {
                        @Override
                        public void onClickC(View view) {
                            sessionUsuario.logoutUser();

                            flag_trigger_logout = true;
                            mDrawerLayout.closeDrawers();
                        }
                    });
                    break;

            }

            if(add_option) {
                // Drawable left
                if(img_icon!=null)btn_opcion.setCompoundDrawablesWithIntrinsicBounds(img_icon, null, null, null);

                /** set item content in view **/
                mDrawerList.addView(opcion_panel);
            }
        }

        flagPanelOption = true;
    }

    /**
     *
     * Crea el panel en la izq escondido
     *
     * */
    public void crearPanel(){

        mActionBar = getSupportActionBar();

        mConfigList = getResources().getStringArray(R.array.lista_opts_panel);
        mDrawerList = (LinearLayout) findViewById(R.id.left_drawer_lista);

        if(!flagPanelOption){
            populate_panel();
        }

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setHomeButtonEnabled(true);

        mToggle = new ActionBarDrawerToggle(this,mDrawerLayout,mToolbar,R.string.panel_open,R.string.panel_close){


            /** Called when a drawer has settled in a completely closed state. **/
            public void onDrawerClosed(View view) {
                if(flag_trigger_logout){
                    flag_trigger_logout = false;

                    Intent intentLogin = new Intent(JosStoreBaseActivity.this,LoginActivity.class);
                    intentLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                            Intent.FLAG_ACTIVITY_CLEAR_TASK |
                            Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intentLogin);
                    finish();
                }
            }

            /** Called when a drawer has settled in a completely open state. **/
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                (mDrawerLayout.getParent()).requestLayout();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
            }


        };

        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();

        backButtonActive = false;
    }

    public void actionbarTitleImgOffset(int leftdp){
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar_jstore);
        if( toolbar != null ) {
            /** Quito el titulo **/
            TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
            mTitle.setVisibility(TextView.GONE);

            /** Muestro el logo **/
            ImageView mTitleLogo = (ImageView) toolbar.findViewById(R.id.title_image);

            if( mTitleLogo != null ) {
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mTitleLogo.getLayoutParams();
                params.setMargins(-(int) UtilsJStore.convertDpToPixel(leftdp, this), (int) UtilsJStore.convertDpToPixel(1, this), 0, 0);
                if (Build.VERSION.SDK_INT >= 17) {
                    params.setMarginStart(-(int) UtilsJStore.convertDpToPixel(leftdp, this));
                }
                mTitleLogo.setLayoutParams(params);
                mTitleLogo.requestLayout();
            }
        }

    }

    /**
     *
     * Quitar el titulo del action bar
     *
     * */
    public void offActionbarTitle(){
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar_jstore);
        if( toolbar != null ) {
            /** Quito el titulo **/
            TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
            mTitle.setVisibility(TextView.GONE);

            /** Muestro el logo **/
            ImageView mTitleLogo = (ImageView) toolbar.findViewById(R.id.title_image);

            if( mTitleLogo != null ) {
                mTitleLogo.setVisibility(ImageView.VISIBLE);
            }

        }

    }

    public void setActionbarTitle(String title){
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar_jstore);
        if( toolbar != null ) {
            /** Le doy titulo al fragment **/
            TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);

            if( mTitle != null ) {
                mTitle.setText(title);
                mTitle.setVisibility(TextView.VISIBLE);
            }

            /** Quito el logo de degusta del fragment **/
            ImageView mTitleLogo = (ImageView) toolbar.findViewById(R.id.title_image);

            if( mTitleLogo != null ) {
                mTitleLogo.setVisibility(ImageView.GONE);
            }

        }
    }

    public void alignTitleStart(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_jstore);

        if( toolbar != null ) {
            TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);

            if( mTitle != null && mTitle.getGravity() != Gravity.START ) {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                params.weight = 1;
                params.leftMargin = 0;

                mTitle.setGravity(Gravity.START);
                mTitle.setGravity(Gravity.CENTER_VERTICAL);
                mTitle.setLayoutParams(params);
            }
        }

    }

    public void activeDisplayBack(){
        mActionBar = getSupportActionBar();

        if(mActionBar != null){
            mActionBar.setHomeButtonEnabled(true);
            mActionBar.setDisplayHomeAsUpEnabled(true);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_jstore);

        if(toolbar!=null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }

    /**
     *
     * Mostrar el botón de back en el actionbar
     *
     * */
    public void shouldDisplayHomeUp(){
        backButtonActive = true;
        mActionBar = getSupportActionBar();

        if(mActionBar != null) {
            mActionBar.setHomeButtonEnabled(false);
            mActionBar.setHomeButtonEnabled(true);
            mActionBar.setDisplayHomeAsUpEnabled(false);
            mActionBar.setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindDrawables(getWindow().getDecorView().getRootView());
        System.gc();
    }

    /**
     *
     * Metodo encargado de borrar los drawables para evitar aumento de memoria
     *
     * */
    public static void unbindDrawables(View view) {
        if(view != null ) {
            if (view instanceof ImageView) {
                ImageView imageView = (ImageView) view;
                imageView.setImageBitmap(null);
            } else if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                for (int i = 0; i < viewGroup.getChildCount(); i++)
                    unbindDrawables(viewGroup.getChildAt(i));

                if (!(view instanceof AdapterView))
                    viewGroup.removeAllViews();
            }
        }
    }


}
