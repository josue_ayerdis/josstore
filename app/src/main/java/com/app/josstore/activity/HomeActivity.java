package com.app.josstore.activity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import com.app.josstore.R;
import com.app.josstore.adapter.HomePageAdapter;
import com.app.josstore.lib.SlidingTabLayout;
import com.app.josstore.services.CheckBids;

public class HomeActivity extends JosStoreBaseActivity {

    ViewPager viewPager;
    SlidingTabLayout slidingTabLayout;
    HomePageAdapter pagerAdapter;

    Intent mServiceIntentCB;
    private CheckBids mSensorServiceCB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        baseCreate(savedInstanceState);
        crearPanel();
        offActionbarTitle();
        actionbarTitleImgOffset(56);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        slidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);

        pagerAdapter = new HomePageAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        slidingTabLayout.setViewPager(viewPager);

        // Servicio que mira las pujas para ver cual termino y ver el ganador
        mSensorServiceCB = new CheckBids(HomeActivity.this);
        mServiceIntentCB = new Intent(HomeActivity.this, mSensorServiceCB.getClass());
        startService(mServiceIntentCB);
    }

    @Override
    protected void onDestroy() {
        stopService(mServiceIntentCB);
        super.onDestroy();
    }
}
