package com.app.josstore.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.app.josstore.R;
import com.app.josstore.data.DataBaseManager;
import com.app.josstore.dialogFragment.PujarDialog;
import com.app.josstore.lib.OnclickCustom;
import com.app.josstore.lib.UtilsJStore;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ItemActivity extends JosStoreBaseActivity {

    int id_item;
    String name_item = "";
    DataBaseManager db;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    long startTime;

    TextView tw_name_item;
    TextView tw_description;
    TextView tw_price;
    TextView tw_bids;
    TextView tw_time;
    ImageView iw_item;
    Button btn_pujar;

    public static final String ACTION_UPDATE_ITEM = "ItemActivity.ACTION_UPDATE_ITEM";
    UpdateReceiver updateReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);

        baseCreate(savedInstanceState);

        Intent intent = getIntent();
        id_item = intent.getIntExtra("id_item",0);
        name_item = intent.getStringExtra("name_item");


        if (getSupportActionBar() != null) getSupportActionBar().setTitle("");
        setActionbarTitle(name_item);
        alignTitleStart();
        activeDisplayBack();

        /** Update intent **/
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_UPDATE_ITEM);
        updateReceiver = new UpdateReceiver();
        registerReceiver(updateReceiver,filter);

        tw_name_item = (TextView) findViewById(R.id.tw_name_item);
        tw_description = (TextView) findViewById(R.id.tw_description);
        tw_price = (TextView) findViewById(R.id.tw_price);
        tw_bids = (TextView) findViewById(R.id.tw_bids);
        tw_time = (TextView) findViewById(R.id.tw_time);
        iw_item = (ImageView) findViewById(R.id.iw_item);
        btn_pujar = (Button) findViewById(R.id.btn_pujar);


        db = new DataBaseManager(this);

        populate_item();
    }


    public void populate_item(){
        if(id_item>0) {
            Cursor cursor = db.get_item(id_item);

            if ( cursor!=null && cursor.moveToFirst() ){

                int id_user = cursor.getInt(cursor.getColumnIndex(DataBaseManager.TBI_ID_USER));
                String fecha = cursor.getString(cursor.getColumnIndex(DataBaseManager.TBI_DATE_END));
                final Double precio = cursor.getDouble(cursor.getColumnIndex(DataBaseManager.TBI_PRICE));
                String foto_path = cursor.getString(cursor.getColumnIndex(DataBaseManager.TBI_IMG));
                String desc = cursor.getString(cursor.getColumnIndex(DataBaseManager.TBI_DESC));
                int bids = cursor.getInt(cursor.getColumnIndex("bids"));

                tw_description.setText(desc);
                tw_price.setText("$"+precio);

                if(bids>0){
                    if(bids>1) {
                        tw_bids.setText(bids + " postores");
                    }else{
                        tw_bids.setText(bids + " postor");
                    }

                    tw_bids.setVisibility(View.VISIBLE);
                }else{
                    tw_bids.setVisibility(View.GONE);
                }

                if(foto_path!=null && !foto_path.isEmpty()) {
                    //Log.v("file_img", foto_path);
                    Bitmap img = UtilsJStore.getImageLocal(foto_path);
                    if (img != null) {
                        iw_item.setImageBitmap(img);
                    }
                }

                try {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(sdf.parse(fecha));
                    long millis = cal.getTimeInMillis();
                    startTime = System.currentTimeMillis();

                    new CountDownTimer(millis, 1000) {

                        public void onTick(long millisUntilFinished) {
                            startTime = startTime-1;
                            Long serverUptimeSeconds = (millisUntilFinished - startTime) / 1000;

                            long daysLeft = serverUptimeSeconds / 86400;
                            long hoursLeft = (serverUptimeSeconds % 86400) / 3600;
                            long minutesLeft = ((serverUptimeSeconds % 86400) % 3600) / 60;

                            if(daysLeft>0) {
                                if(daysLeft>1) {
                                    tw_time.setText(daysLeft + " dias " + hoursLeft + " hrs");
                                }else{
                                    tw_time.setText(daysLeft + " dia " + hoursLeft + " hrs");
                                }
                            }else{
                                tw_time.setText(hoursLeft + " hrs " + minutesLeft + " mins");
                            }
                        }

                        public void onFinish() {
                            tw_time.setText("Finalizada");
                            btn_pujar.setVisibility(View.GONE);
                        }

                    }.start();

                    tw_time.setVisibility(View.VISIBLE);
                }catch (ParseException e){
                    tw_time.setVisibility(View.GONE);
                    e.printStackTrace();
                }


                if(sessionUsuario.isLoggedIn() && sessionUsuario.getUserInstance().getId() == id_user ){
                    btn_pujar.setOnClickListener(null);
                    btn_pujar.setVisibility(View.GONE);
                }else{
                    btn_pujar.setVisibility(View.VISIBLE);

                    btn_pujar.setOnClickListener(new OnclickCustom() {
                        @Override
                        public void onClickC(View view) {

                            DialogFragment pujaDialog = new PujarDialog();

                            Bundle args = new Bundle();
                            args.putInt("id_item", id_item);
                            args.putDouble("precio_item", precio);
                            pujaDialog.setArguments(args);

                            /** Open dialog **/
                            getSupportFragmentManager().beginTransaction().add(pujaDialog, "dialog_puja").commitAllowingStateLoss();

                        }
                    });
                }


            }
        }
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(updateReceiver);
        super.onDestroy();
    }

    class UpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction()!=null && intent.getAction().equals(ACTION_UPDATE_ITEM) && db!=null) {
                populate_item();
            }
        }
    }


}
