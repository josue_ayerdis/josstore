package com.app.josstore.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.app.josstore.R;
import com.app.josstore.data.DataBaseManager;
import com.app.josstore.lib.OnclickCustom;
import com.app.josstore.lib.SessionUsuario;
import com.app.josstore.lib.UtilsJStore;

public class LoginActivity extends JosStoreBaseActivity {

    SessionUsuario sessionUsuario;

    EditText et_usuario;
    EditText et_password;
    Button btn_entrar;
    TextView tw_registrate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        baseCreate(savedInstanceState);

        sessionUsuario = new SessionUsuario(this);

        if(sessionUsuario.isLoggedIn()){
            Intent ihome = new Intent(this,HomeActivity.class);
            startActivity(ihome);
            finish();
        }else{

            et_usuario = (EditText) findViewById(R.id.et_username);
            et_password = (EditText) findViewById(R.id.et_password);
            btn_entrar = (Button) findViewById(R.id.btn_entrar);
            tw_registrate = (TextView) findViewById(R.id.tw_registrate);

            et_usuario.addTextChangedListener(new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    check_fields();
                }

                @Override
                public void afterTextChanged(Editable s) {
                }

            });

            et_password.addTextChangedListener(new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    check_fields();
                }

                @Override
                public void afterTextChanged(Editable s) {
                }

            });


            btn_entrar.setOnClickListener(new OnclickCustom() {
                @Override
                public void onClickC(View view) {
                    String user = et_usuario.getText().toString();
                    String password = et_password.getText().toString();
                    validar_user(user,password);
                }
            });


            tw_registrate.setOnClickListener(new OnclickCustom() {
                @Override
                public void onClickC(View view) {
                    Intent ireg = new Intent(LoginActivity.this,RegisterActivity.class);
                    startActivity(ireg);
                }
            });

        }
    }

    private void validar_user(String user,String password){
        DataBaseManager db = new DataBaseManager(LoginActivity.this);
        if(db.login_usuario(user,password)){
            Intent ihome = new Intent(LoginActivity.this,HomeActivity.class);
            startActivity(ihome);
            finish();
        }else{
            UtilsJStore.show_accept_msg(LoginActivity.this,"Usuario o contraseña incorrectos");
        }
    }

    public void check_fields(){

        String text_username = et_usuario.getText().toString();
        String text_password = et_password.getText().toString();

        if( !text_username.isEmpty() && !text_password.isEmpty() ){
            btn_entrar.setEnabled(true);
        }else{
            btn_entrar.setEnabled(false);
        }
    }

}
