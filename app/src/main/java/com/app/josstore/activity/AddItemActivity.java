package com.app.josstore.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.josstore.R;
import com.app.josstore.data.DataBaseManager;
import com.app.josstore.dialogFragment.ChoosePhotoPickerDialog;
import com.app.josstore.fragment.MisSubastasFragment;
import com.app.josstore.fragment.SubastasFragment;
import com.app.josstore.lib.OnclickCustom;
import com.app.josstore.lib.SessionUsuario;
import com.app.josstore.lib.UtilsJStore;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import java.util.Calendar;

public class AddItemActivity extends JosStoreBaseActivity implements TimePickerDialog.OnTimeSetListener,DatePickerDialog.OnDateSetListener {

    Button btn_guardar;
    View btn_fecha;
    View btn_hora;

    EditText et_nombre;
    EditText et_precio;
    EditText et_descripcion;

    Button btn_foto;
    ImageView iw_preview;

    String path_file_selected = null;

    TextView tw_fecha;
    TextView tw_hora;
    boolean is_selected_hour;
    boolean is_selected_date;
    String selected_hour;
    String selected_date;

    int id_user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);

        baseCreate(savedInstanceState);

        if (getSupportActionBar() != null) getSupportActionBar().setTitle("");
        setActionbarTitle("Subastar Artículo");
        alignTitleStart();
        activeDisplayBack();

        sessionUsuario = new SessionUsuario(this);
        id_user = sessionUsuario.getUserInstance().getId();

        btn_guardar = (Button) findViewById(R.id.btn_save);
        btn_fecha = findViewById(R.id.btn_fecha);
        btn_hora = findViewById(R.id.btn_hora);

        et_nombre = (EditText) findViewById(R.id.et_nombre);
        et_precio = (EditText) findViewById(R.id.et_precio);
        et_descripcion = (EditText) findViewById(R.id.et_descripcion);

        btn_foto = (Button) findViewById(R.id.btn_foto);
        iw_preview = (ImageView) findViewById(R.id.iw_preview);

        tw_fecha = (TextView) findViewById(R.id.tw_fecha);
        tw_hora = (TextView) findViewById(R.id.tw_hora);

        btn_fecha.setOnClickListener(new OnclickCustom() {
            @Override
            public void onClickC(View view) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        AddItemActivity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );

                Calendar today = Calendar.getInstance();
                //today.add(Calendar.DATE,1);
                dpd.setMinDate(today);

                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        btn_hora.setOnClickListener(new OnclickCustom() {
            @Override
            public void onClickC(View view) {
                Calendar now = Calendar.getInstance();
                TimePickerDialog tpd = TimePickerDialog.newInstance(
                        AddItemActivity.this,
                        now.get(Calendar.HOUR_OF_DAY),
                        now.get(Calendar.MINUTE),
                        true
                );
                tpd.show(getFragmentManager(), "TimePickerDialog");
            }
        });

        btn_foto.setOnClickListener(new OnclickCustom() {
            @Override
            public void onClickC(View view) {
                DialogFragment dialogPickPhoto = new ChoosePhotoPickerDialog();
                getSupportFragmentManager().beginTransaction().add(dialogPickPhoto,"dialog_choose_picker_photo").commitAllowingStateLoss();
            }
        });


        btn_guardar.setOnClickListener(new OnclickCustom() {
            @Override
            public void onClickC(View view) {
                String nombre_i = et_nombre.getText().toString();
                String precio = et_precio.getText().toString();
                String descripcion = et_descripcion.getText().toString();

                if( is_selected_date && is_selected_hour && !nombre_i.isEmpty() && !precio.isEmpty() && !descripcion.isEmpty() && path_file_selected!=null && !path_file_selected.isEmpty() ){
                    String join_date_time = selected_date+" "+selected_hour;
                    new AddItemTask(nombre_i,precio,descripcion,path_file_selected,join_date_time).execute("");
                }else{
                    UtilsJStore.show_accept_msg(AddItemActivity.this,"Debes llenar todos los campos");
                }

            }
        });
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        selected_hour = hourOfDay+":"+minute+":"+second;
        is_selected_hour = true;
        tw_hora.setText(selected_hour);
        Log.i("time",selected_hour);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        selected_date = year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
        is_selected_date = true;
        tw_fecha.setText(selected_date);
        Log.i("time",selected_date);
    }

    public void callBackPicture(String filename){
        if(filename!=null && !filename.isEmpty()) {
            Bitmap img = UtilsJStore.getImageLocal(filename);
            if(img!=null) {
                iw_preview.setImageBitmap(img);
                path_file_selected = filename;
            }
        }
    }

    public class AddItemTask extends AsyncTask<String, Integer, Boolean> {

        String nombre_i;
        String precio;
        String descripcion;
        String foto;
        String fecha;

        AddItemTask(String nombre_i,String precio,String descripcion,String foto,String fecha){
            this.nombre_i = nombre_i;
            this.precio = precio;
            this.descripcion = descripcion;
            this.foto = foto;
            this.fecha = fecha;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            UtilsJStore.show_loading_dialog(AddItemActivity.this,"Guardando..");

        }

        @Override
        protected Boolean doInBackground(String... server) {
            DataBaseManager db = new DataBaseManager(AddItemActivity.this);

            return db.insert_item(id_user,nombre_i,precio,descripcion,foto,fecha);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            UtilsJStore.hide_loading_dialog();

            if(result){

                //Update feed subastas
                Intent i_update_s = new Intent(SubastasFragment.ACTION_UPDATE_SUBASTAS);
                sendBroadcast(i_update_s);

                // update mis subastas
                Intent i_mis_subastas = new Intent(MisSubastasFragment.ACTION_UPDATE_MIS_SUBASTAS);
                sendBroadcast(i_mis_subastas);

                UtilsJStore.show_accept_msg(AddItemActivity.this, "Subasta creada correctamente", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
            }else{
                UtilsJStore.show_accept_msg(AddItemActivity.this,"Hubo un problema insertando el artículo intentalo nuevamente");
            }

        }


    }
}
