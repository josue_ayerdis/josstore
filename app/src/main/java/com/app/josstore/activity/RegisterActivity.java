package com.app.josstore.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.app.josstore.R;
import com.app.josstore.data.DataBaseManager;
import com.app.josstore.lib.OnclickCustom;
import com.app.josstore.lib.UtilsJStore;

public class RegisterActivity extends JosStoreBaseActivity {

    Button btn_register;
    EditText et_username;
    EditText et_email;
    EditText et_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        baseCreate(savedInstanceState);

        btn_register = (Button) findViewById(R.id.btn_register);
        et_email = (EditText) findViewById(R.id.et_email);
        et_username = (EditText) findViewById(R.id.et_username);
        et_password = (EditText) findViewById(R.id.et_password);

        btn_register.setOnClickListener(new OnclickCustom() {
            @Override
            public void onClickC(View view) {
                String email = et_email.getText().toString();
                String username = et_username.getText().toString();
                String password = et_password.getText().toString();

                DataBaseManager db = new DataBaseManager(RegisterActivity.this);

                if( email.isEmpty() || username.isEmpty() || password.isEmpty() ){
                    UtilsJStore.show_accept_msg(RegisterActivity.this,"Debe llenar todos los campos");
                }else{
                    if(db.existe_usuario(email,username)){
                        UtilsJStore.show_accept_msg(RegisterActivity.this,"Este usuario ya existe");
                    }else{
                        if(db.registrar_usuario(username,email,password)){
                            UtilsJStore.show_accept_msg(RegisterActivity.this, "Usuario Registrado exitosamente", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent ihome = new Intent(RegisterActivity.this,HomeActivity.class);
                                    ihome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(ihome);
                                }
                            });
                        }
                    }
                }
            }
        });
    }
}
