package com.app.josstore.dialogFragment;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.app.josstore.R;
import com.app.josstore.activity.ItemActivity;
import com.app.josstore.activity.JosStoreBaseActivity;
import com.app.josstore.activity.MisPujasActivity;
import com.app.josstore.data.DataBaseManager;
import com.app.josstore.fragment.SubastasFragment;
import com.app.josstore.lib.SessionUsuario;
import com.app.josstore.lib.UtilsJStore;

public class PujarDialog extends DialogFragment {

    View main_view;
    int id_item;
    double precio_item;
    JosStoreBaseActivity mActivity;
    DataBaseManager db;
    SessionUsuario sessionUsuario;
    int id_user;
    boolean is_update;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivity = (JosStoreBaseActivity) getActivity();
        db = new DataBaseManager(mActivity);
        sessionUsuario = new SessionUsuario(mActivity);

        if(sessionUsuario.isLoggedIn()){
            id_user = sessionUsuario.getUserInstance().getId();
        }

        if(getArguments()!=null){
            id_item = getArguments().getInt("id_item");
            precio_item = getArguments().getDouble("precio_item");
            is_update = getArguments().getBoolean("is_update",false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        main_view = inflater.inflate(R.layout.dialog_fragment_pujar, container);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        Button btn_cancelar = (Button) main_view.findViewById(R.id.btn_cancelar);

        btn_cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissAllowingStateLoss();
            }
        });

        Button btn_enviar = (Button) main_view.findViewById(R.id.btn_enviar);
        final EditText et_precio = (EditText) main_view.findViewById(R.id.et_precio);

        btn_enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final double precio = Double.parseDouble(et_precio.getText().toString());

                if(precio >= precio_item ){
                    // crear puja
                    if(db!=null && id_user>0 && id_item>0){
                        if(db.existe_puja(id_user,id_item)){

                            if(is_update){
                                if (db.update_puja(id_user, id_item, precio)) {
                                    //Update mis pujas
                                    Intent i_update_mp = new Intent(MisPujasActivity.ACTION_UPDATE_PUJAS);
                                    mActivity.sendBroadcast(i_update_mp);

                                    dismissAllowingStateLoss();
                                    UtilsJStore.show_accept_msg(mActivity, "Puja actualizada");
                                } else {
                                    UtilsJStore.show_accept_msg(mActivity, "Hubo un problema al actualizar la puja");
                                }
                            }else {
                                //actualizo la puja
                                new AlertDialog.Builder(mActivity)
                                        .setTitle("La puja existe")
                                        .setMessage("¿Desea actualizar su puja anterior?")
                                        .setPositiveButton("SI", new DatePickerDialog.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (UtilsJStore.isFirstClick()) {
                                                    if (db.update_puja(id_user, id_item, precio)) {
                                                        dismissAllowingStateLoss();
                                                        UtilsJStore.show_accept_msg(mActivity, "Puja actualizada");
                                                    } else {
                                                        UtilsJStore.show_accept_msg(mActivity, "Hubo un problema al actualizar la puja");
                                                    }
                                                }
                                            }
                                        })
                                        .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dismissAllowingStateLoss();
                                            }
                                        })
                                        .show();
                            }

                        }else {

                            // nueva puja
                            if (db.crear_puja(id_user, id_item, precio)) {
                                //Update feed subastas
                                Intent i_update_s = new Intent(SubastasFragment.ACTION_UPDATE_SUBASTAS);
                                mActivity.sendBroadcast(i_update_s);

                                //Update item
                                Intent i_update_i = new Intent(ItemActivity.ACTION_UPDATE_ITEM);
                                mActivity.sendBroadcast(i_update_i);

                                dismissAllowingStateLoss();
                                UtilsJStore.show_accept_msg(mActivity, "Puja creada");
                            } else {
                                UtilsJStore.show_accept_msg(mActivity, "Hubo un problema al crear la puja");
                            }

                        }
                    }
                }else{
                    UtilsJStore.show_accept_msg(mActivity,"La puja debe ser mayor al precio del producto");
                }
            }
        });



        return main_view;
    }

}
