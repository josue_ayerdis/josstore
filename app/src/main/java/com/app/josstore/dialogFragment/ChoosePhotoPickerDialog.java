package com.app.josstore.dialogFragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import com.app.josstore.R;
import com.app.josstore.activity.AddItemActivity;
import com.app.josstore.lib.UtilsJStore;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


/**
 * Created by Degusta-JA on 08/27/2015.
 */
public class ChoosePhotoPickerDialog extends DialogFragment {

    View main_view;
    public static final int PICK_IMAGE = 1;
    public static final int PICK_CAMERA = 2;
    public static final int MY_PERMISSIONS_REQUEST_READ_STORAGE = 8;
    public static final int MY_PERMISSIONS_REQUEST_READ_STORAGE_CAM = 9;
    AddItemActivity addItemActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getActivity() instanceof AddItemActivity){
            addItemActivity = (AddItemActivity) getActivity();
        }
    }

    String imageFilePath;
    private File createImageFile() throws IOException {
        if(getActivity()!=null) {
            String timeStamp =
                    new SimpleDateFormat("yyyyMMdd_HHmmss",
                            Locale.getDefault()).format(new Date());
            String imageFileName = "IMG_" + timeStamp + "_";
            File storageDir =
                    getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            File image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );

            imageFilePath = image.getAbsolutePath();

            return image;
        }else {
            return null;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        main_view = inflater.inflate(R.layout.dialog_fragment_picker_photo, container);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        Button btn_camara = (Button) main_view.findViewById(R.id.btn_camara);

        btn_camara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                        if(intent.resolveActivity(getActivity().getPackageManager()) != null) {
                            //Create a file to store the image
                            File photoFile = null;

                            try {
                                photoFile = createImageFile();
                                Log.i("file",imageFilePath);
                            } catch (IOException ex) {
                                // Error occurred while creating the File
                            }

                            if (photoFile != null) {
                                Uri photoURI = FileProvider.getUriForFile(getActivity(),"com.app.josstore.provider", photoFile);
                                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                                        photoURI);
                                // start the image capture Intent
                                startActivityForResult(intent, PICK_CAMERA);
                            }
                        }


                    }else{
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA},MY_PERMISSIONS_REQUEST_READ_STORAGE_CAM);
                    }
                }else{
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    if(intent.resolveActivity(getActivity().getPackageManager()) != null) {
                        //Create a file to store the image
                        File photoFile = null;

                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            // Error occurred while creating the File
                        }

                        if (photoFile != null) {
                            Uri photoURI = FileProvider.getUriForFile(getActivity(),"com.app.josstore.provider", photoFile);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                                    photoURI);
                            // start the image capture Intent
                            startActivityForResult(intent, PICK_CAMERA);
                        }
                    }
                }
            }
        });


        Button btn_galeria = (Button) main_view.findViewById(R.id.btn_galeria);

        btn_galeria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Intent intent = new Intent();
                        intent.setType("image/jpeg");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Seleccione una foto"), PICK_IMAGE);
                    } else {
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_STORAGE);
                    }
                } else {
                    Intent intent = new Intent();
                    intent.setType("image/jpeg");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Seleccione una foto"), PICK_IMAGE);
                }
            }
        });

        return main_view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK && data!=null){
            Uri selectedImage = data.getData();
            String filename = UtilsJStore.getPath(getActivity(), selectedImage);
            // Envio la información al dialogo de actualizar
            if(addItemActivity!=null) {
                addItemActivity.callBackPicture(filename);
            }
        }

        if(requestCode == PICK_CAMERA && resultCode == Activity.RESULT_OK ){
            // Envio la información al dialogo de actualizar
            if(addItemActivity!=null) {
                if(imageFilePath!=null) {
                    addItemActivity.callBackPicture(imageFilePath);
                }
            }
        }


        dismissAllowingStateLoss();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay!
                    Intent intent = new Intent();
                    intent.setType("image/jpeg");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Seleccione una foto"), PICK_IMAGE);
                }else{
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            case MY_PERMISSIONS_REQUEST_READ_STORAGE_CAM: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay!

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    if(intent.resolveActivity(getActivity().getPackageManager()) != null) {
                        //Create a file to store the image
                        File photoFile = null;

                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            // Error occurred while creating the File
                        }

                        if (photoFile != null) {
                            Uri photoURI = FileProvider.getUriForFile(getActivity(),"com.app.josstore.provider", photoFile);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                                    photoURI);
                            // start the image capture Intent
                            startActivityForResult(intent, PICK_CAMERA);
                        }
                    }

                }else{
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }
}

